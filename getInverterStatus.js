"use strict"
const api = require('growatt')
// Forked from https://github.com/PLCHome/growatt/tree/master 

const user = 'Your Email';
const passwort = 'Your PAssword';
const options={plantId:"Plant ID",weather:false}

async function getInverterStatus() {
  const growatt = new api({})
  let login = await growatt.login(user, passwort).catch(e => { console.log(e) })
  console.log('login:', login)
  let getAllPlantData = await growatt.getAllPlantData(options).catch(e => { console.log(e) })
  // Uncomment the line below in order to find Plant ID && Plant ID
  //console.log('getAllPlatData:', JSON.stringify(getAllPlantData, null, ' '));

  if (getAllPlantData && getAllPlantData["Plant ID"] && getAllPlantData["Plant ID"].devices) {
    const devices = getAllPlantData["Plant ID"].devices;
    const deviceName = "Device Name";

    if (devices.hasOwnProperty(deviceName)) {
      const deviceData = devices[deviceName].deviceData;
      const statusData = devices[deviceName].statusData;
      const totalData = devices[deviceName].totalData;
      const historyLast = devices[deviceName].historyLast;
      const eTotal = deviceData.eTotal;
      const eToday = deviceData.eToday;
      const eMonth = deviceData.eMonth;
      console.log('---------------------------');
      console.log('Device:', deviceName);
      console.log('PPV generated Today:', eToday,'kWh');
      console.log('PPV generated this Month:', eMonth,'kWh');
      console.log('PPV Total:', eTotal,'kWh');
      console.log('---------------------------');
      const ppv = statusData.ppv;
      const SOC = statusData.SOC;
      const chargePower = statusData.chargePower;
      const pLocalLoad = statusData.pLocalLoad;
      const pactogrid = statusData.pactogrid;
      const pactouser = statusData.pactouser;
      console.log('PV Inverter Power:', ppv,'kW');
      console.log('Battery:', SOC,'%');
      console.log('Battery Charging:', chargePower,'kW');
      console.log('House consumption :', pLocalLoad,'kW');
      console.log('Exporing to grid:', pactogrid,'kW');
      console.log('Importing from grid:', pactouser,'kW');
      console.log('---------------------------');
      const etoGridToday = totalData.etoGridToday;
      const etogridTotal = totalData.etogridTotal;
      const epvInverterToday = totalData.epvInverterToday;
      const batteryTemperature = historyLast.batteryTemperature;
      console.log('Exported to grid today:', etoGridToday,'kWh');
      console.log('Exported to grid total:', etogridTotal,'kWh');
      //console.log('Total PV Generated today:', epvInverterToday,'kWh');
      console.log('Battery Temp:', batteryTemperature,'ºC');
      console.log('---------------------------');
    } else {
      console.log(`Device '${deviceName}' not found.`);
    }
  }

  let logout = await growatt.logout().catch(e => { console.log(e) })
  console.log('logout:', logout)
}

getInverterStatus()
